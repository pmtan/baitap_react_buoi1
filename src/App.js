import logo from './logo.svg';
import './App.css';
import Header from './Bai Tap React 1/Header';
import Body from './Bai Tap React 1/Body';
import Footer from './Bai Tap React 1/Footer';

function App() {
  return (
    <div className="App">
      <Header/>
      <Body/>
      <Footer/>
    </div>
  );
}

export default App;
