import React, { Component } from "react";
import img from "./bg.png";
export default class Item extends Component {
  render() {
    return (
      <div>
        <div className="card m-3">
          <img className="card-img-top" src={img} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
          </div>
          <div className="bg-light border">
            <a href="#" className="btn btn-primary my-2">
              Go somewhere
            </a>
          </div>
        </div>
      </div>
    );
  }
}
